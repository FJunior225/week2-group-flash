require_relative '../view/view'
require_relative '../model/deck'
require 'pry'
class Controller

	def initialize
		@my_deck = Deck.new
	end

	def run
		View.menu
		sleep(1)
		input = View.get_user_guess.downcase
		case input
		when 'instructions'
			View.instructions			
		when 'nighthawk'
			@my_deck.add_cards_bulk('/Users/FJ_Junior/Development/code/phase-0-curriculum/phase-1/flash/week2-group-flash/csv/nighthawk.csv')
		when 'otter'
			@my_deck.add_cards_bulk('/Users/FJ_Junior/Development/code/phase-0-curriculum/phase-1/flash/week2-group-flash/csv/otter.csv')
		when 'raccoon'
			@my_deck.add_cards_bulk('/Users/FJ_Junior/Development/code/phase-0-curriculum/phase-1/flash/week2-group-flash/csv/raccoon.csv')
		else 
			@my_deck.add_cards_bulk('/Users/FJ_Junior/Development/code/phase-0-curriculum/phase-1/flash/week2-group-flash/model/flashcard.csv')
		end

		run_original_deck(@my_deck.deck)

		if View.end_game == 'Y'
			run_original_deck(@my_deck.incorrect_pile)
			# binding.pry
		end
		View.goodbye_game

	end

		def run_original_deck(chosen_deck)
			chosen_deck.each do |card|
				View.show_definition(card.definition)
				guess = View.get_user_guess.downcase
				guess_counter = 1 
				until card.answer == guess || guess_counter == 3
					guess_counter += 1
					View.show_guess(guess)
					View.incorrect_answer
					@my_deck.push_incorrect_cards(card) unless @my_deck.incorrect_pile.include?(card)
					guess = View.get_user_guess.downcase
					View.incorrect_answer
				end
				if card.answer == guess && guess_counter == 1
					View.correct_answer
					@my_deck.push_correct_cards(card) unless @my_deck.correct_pile.include?(card)
				else 
					View.correct_answer
				end
			end
		end
end