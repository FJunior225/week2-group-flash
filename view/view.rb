require 'colorize'

module View
	def self.menu
		puts "Welcome to Flash Cards! To play, just enter the correct term for each definition."
		puts 'Please select a deck!'
		puts "-- Nighthawk"
		puts "-- Otter"
		puts "-- Raccoon"
		puts '-- Instructions for instructions'
	end

	def self.instructions
		puts "You have 3 guesses to answer a question correctly."
	end

	def self.show_definition(definition)
		puts "Definition:"
		puts definition
		puts
	end

	def self.show_guess(guess)
		puts "Guess: #{guess}"
		puts
	end

	def self.get_user_guess
		gets.chomp
	end

	def self.correct_answer
		puts "Correct!"
		puts
	end

	def self.incorrect_answer
		puts "Incorrect. Please try again!"
		puts
	end	

	def self.end_game 
		puts "You've completed this deck. Would you like to review cards in the deck you got wrong? Y/N?"
		gets.chomp
	end 

	def self.goodbye_game
		puts "Congratulations, you've learned a lot today!"
	end
end
