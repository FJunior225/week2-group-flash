class Card
attr_reader :answer, :definition

  def initialize(args = {})
    @answer = (args.fetch(:answer)).downcase
    @definition = args.fetch(:definition)
  end


end
