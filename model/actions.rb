require "csv"
require_relative 'card'

module Actions

  def add_cards_bulk(filepath)
    options = {headers: true, header_converters: :symbol}
    CSV.foreach(filepath, options) do |row|
      @deck << Card.new(row)
    end
  end

  def compare(user_answer, card_object)
    card_object.answer == user_answer
  end

  def push_correct_cards(card_object)
  	@correct_pile << card_object
  end

  def push_incorrect_cards(card_object)
  	@incorrect_pile << card_object
  end
end