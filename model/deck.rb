require_relative 'actions'

class Deck
  include Actions
  attr_reader :deck, :correct_pile, :incorrect_pile

  def initialize
    @deck = []
    @correct_pile = []
    @incorrect_pile = []
  end


end