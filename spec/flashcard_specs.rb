require_relative '../model/card'
require_relative '../model/deck'


describe Card do
  let(:example) {Card.new(answer: "string", definition: "a line of code")}
  # before(:each) { example.add_card(answer: "string", definition: "a line of code") }

  it "should have an answer" do
    expect(example.answer).to eq("string")
  end

  it "should have a definition" do
    expect(example.definition).to eq("a line of code")
  end

end

describe Deck do
  let(:deck) {Deck.new}

  it "should instantiate an empty deck" do
    expect(deck.deck).to eq([])
  end

  it "should add cards from csv file to deck" do
    deck.add_cards_bulk("/Users/kentonlin/desktop/week2-group-flash/model/flashcard.csv")
    expect(deck.deck.size).to eq(38)
  end

end

